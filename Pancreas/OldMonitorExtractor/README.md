# ncsa.msc.dailymonitor extractor

Notice: This is not used anymore.

## Overview

Ingests the Daily monitor spreadsheet to the MSC database. This is the old style of the spreadsheets that the researchers used to log daily data.

## Input

A ".xlsx" or ".xls" file containing the daily monitor data.

## Output
MSC database is updated.

## Dependencies

* ###Python modules
  * #####Using virtualenv
You may use the included requirements.txt file along with virtualenv to automatically install all required python libraries in a sandboxed python environment

        # Create a virtual environment  
        $ virtualenv -p=python2.7 ENV
        
        # Activate the virtual environment  
        $ source ENV/bin/activate
          
        # Install the required python modules  
        $ pip install -r requirements.txt        

    When you are finished using your virtual environment, be sure to run the deactivate script to restore your $PATH

        $ deactivate

  * #####System wide
Alternately, you may use the included requirements.txt file to install all required libraries system wide

        $ sudo pip install -r requirements

  * #####Individual packages
  You may also install the individual dependencies listed below with pip using

        $ sudo pip install [package name]

     * **requests**  
[http://docs.python-requests.org/en/latest/](http://docs.python-requests.org/en/latest/)  

     * **pika**  
[https://pypi.python.org/pypi/pika](https://pypi.python.org/pypi/pika)  

        
	* **openpyxl**  
[https://pypi.python.org/pypi/openpyxl](https://pypi.python.org/pypi/openpyxl)

	* **xlrd**  
[https://pypi.python.org/pypi/xlrd](https://pypi.python.org/pypi/xlrd)


	* **pymongo**    
[http://api.mongodb.org/python/current/](http://api.mongodb.org/python/current/)