import xlrd
import datetime
import re

from utilities import *
import traceback

gender_re = re.compile("Female|Male", re.IGNORECASE)
type_re = re.compile("Cyno|Baboon", re.IGNORECASE)
dob_re = re.compile("DOB\\s*[=:]\\s*(\\d*.\\d*.\\d+)")
stz_re = re.compile("STZ\\s*o?n?\\s*(\\d*.\\d*.\\d+)")
ict_re = re.compile("ICT\\s*\\(?.*\\)?\\s*on\\s*(\\d*.\\d*.\\d+)")
ieq_re = re.compile("Total IEQ\\s+=?\\s+(\\d*,?\\d*)")
ieqnorm_re = re.compile("(\\d*,\\d*)\\s+IEQ/kg")
donor_re = re.compile("Donor\\s+(?:N/A)?(\\w*-?\\w*)")
bloodtype_re = re.compile("ABO\\s*=\\s*([A-Z]{1,2}):")

insulin_re = re.compile("(\\d*\.?\\d*)([R|L|N])")

#units regular expressions
units_re = re.compile("\w+?\\s*([\[\(]\\w+?\/?\\w*?[\]\)])", re.IGNORECASE) #units are gotten as (kg), weight or [ng/ml] for Rapamune levels
units_alt_re = re.compile("\w+?\\s*(\\w+?\/{1}\\w*)\Z", re.IGNORECASE) #units are gotten as mg/kg only at end of string

#drug regular expressions
drug_re1 = re.compile("administered\\s*\\d*\\s*(\\w*\\s*)\\s*@\\s*(\\d*\.?\\d*\\s*\\w*\/kg)", re.IGNORECASE)
drug_re2 = re.compile("administered\\s*(\\w*\\s*)\\s*\(.*\)*\\s*at\\s*(\\d*\\s*\\w*\/kg)", re.IGNORECASE)
drug_re3 = re.compile("administered\\s*(\\d*\.?\\d*\\s*\\w*)\\s*of\\s*(\\w*\\s*)", re.IGNORECASE)
drug_re4 = re.compile("administered\\s*(\\d*\.?\\d*\\s*\\w*)\\s*(\\w*\\s*)", re.IGNORECASE)

rapa_re = re.compile("Rapamycin\\s*@\\s*(\\d*\.?\\d*\\s*\\w*\/\\w*)\\s*(?:IM in|IM|in)\\s*(\\w*)", re.IGNORECASE)
rapa_alt_re = re.compile("Rapamycin\\s*@\\s*(\\d*\.?\\d*\\s*\\w*\/\\w*)\\s*(\\w*)", re.IGNORECASE) #alternate without IM

second_dose_re = re.compile("and\\s*(\\d*\.?\\d*\\s*\\w*\/\\w*)\\s*(?:IM in|IM|in)\\s*(\\w*)")
second_dose_alt_re = re.compile("and\\s*(\\d*\.?\\d*\\s*\\w*\/\\w*)\\s*(\\w*)")

fk506_re = re.compile("FK506\\s*@\\s*(\\d*\.?\\d*\\s*\\w*\/\\w*)\\s*(?:IM in|IM|in)\\s*(\\w*)", re.IGNORECASE)
fk506_alternate_re = re.compile("FK506\\s*@\\s*(\\d*\.?\\d*\\s*\\w*\/\\w*)\\s*(\\w*)", re.IGNORECASE)

level_re = re.compile("(\\d*\.?\\d*?)(0*)\\s*(\\w*\/\\w*)")

class parse_sheet_xls:

    def __init__(self, ws, category):
        self.worksheet = ws
        self.category = category
        self.name = ws.name
        self.data_headers_xls = {}
        self.data_units_xls = {}
        self.data =[]

        if ws.nrows == 0 and ws.ncols == 0:
            self.category = ws.name
            return
        
        self.parse_subject_info()

        self.parse_data_info_header()

        self.parse_data()

    def parse_subject_info(self):
        subject_info = self.worksheet.cell(0,0).value
        name, info = subject_info.split(None, 1)
        name.strip();
        self.name = name
        self.subject_dict = {"ID":self.name, "Category" : self.category}
        info = info.strip()

        donor = re.search(donor_re, info)
        if donor:
            self.subject_dict["Islet Donor"] = {"ID":donor.group(1)}

        info = info.split("Donor")

        if len(info) == 2:
            recipient_str = info[0]
            donor_str = info[1].split(")", 1)
            
            if len(donor_str) == 2:
                pass # get things about donor here

            recipient_str = recipient_str + donor_str[-1]
            
            dob = re.search(dob_re, recipient_str)
            if dob:
                self.subject_dict["DOB"] = dob.group(1)

            stz = re.search(stz_re, recipient_str)
            if stz:
                self.subject_dict["STZ"] = stz.group(1)

            ict = re.search(ict_re, recipient_str)
            if ict:
                self.subject_dict["Date of 1st ITX"] = ict.group(1)

            ieq = re.search(ieq_re, recipient_str)
            if ieq:
                self.subject_dict["total IEQ"] = ieq.group(1)
            ieq_norm = re.search(ieqnorm_re, recipient_str)
            if ieq_norm:
                self.subject_dict["IEQ/kg"] = ieq_norm.group(1)

            bloodtype = re.search(bloodtype_re, recipient_str)
            if bloodtype:
                self.subject_dict["ABO"] = bloodtype.group(1)

            gender = re.search(gender_re, recipient_str)
            if gender:
                self.subject_dict["Gender"] = gender.group(0).capitalize()


            monkeytype = re.search(type_re, recipient_str)
            if monkeytype:
                self.subject_dict["Type"] = monkeytype.group(0).capitalize()


    def parse_data_info_header(self):
        for c in range(self.worksheet.ncols):
            cell = self.worksheet.cell(1,c)
            header = cell.value
            units_match = re.search(units_re, header)
            if units_match:
                self.data_units_xls[c] = units_match.group(1)[1:-1] # drop the ()
                header = header.replace(units_match.group(1),"").strip()
            else:
                units_match = re.search(units_alt_re, header)
                if units_match:
                     self.data_units_xls[c] = units_match.group(1)
                     header = header[0:header.index(units_match.group(1))].strip()

            self.data_headers_xls[c] = header


    def parse_data(self):
        #filtering out rows which dont have FBG and PPG values, researchers add lot of rows at end of sheet.
        fbg_key = map_xls.get(self.data_headers_xls[2], self.data_headers_xls[2])
        ppg_key = map_xls.get(self.data_headers_xls[3], self.data_headers_xls[3])
        
        necessary_keys = ["ID", "Date", "Category", sanitize_name(fbg_key), sanitize_name(ppg_key)]
        invalid_cell_types = [xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_ERROR, xlrd.XL_CELL_BLANK]

        for r in range(2,self.worksheet.nrows):
            data_row_dict = {"ID":self.name, "Category":self.category}
            

            for c in range(self.worksheet.ncols):
                cell = self.worksheet.cell(r, c)
                if any(x == cell.ctype for x in invalid_cell_types):
                    continue
                self.process_value_for_dict(data_row_dict, cell, c)
            
            if all(k in data_row_dict for k in necessary_keys) and len(data_row_dict) > len(necessary_keys):
                self.data.append(data_row_dict)

    def process_value_for_dict(self, data_row_dict, cell, c):
        if not self.data_headers_xls.has_key(c):
            return

        cell_value = sanitize_value(cell.value)
        if cell_value == None or cell_value == '': # c==None wont eliminate c = 0 values
            return

        key = self.data_headers_xls[c]
        key = map_xls.has_key(key) and map_xls[key] or key

        if isinstance(key, list):
            if cell.ctype == xlrd.XL_CELL_TEXT:
                self.unpack_insulin_values(data_row_dict, key, cell_value)
        elif key == "Comments":
            if cell.ctype == xlrd.XL_CELL_TEXT:
                self.extract_drug_info(data_row_dict, cell_value)
                data_row_dict[key] = cell_value # add comments to the database 'as is' too
        else:
            if c == 0 and not isinstance(cell_value, float): # getting some unreadable unicode char
                return
            elif c == 0: #Date needs processing from xls files
                date_tuple = xlrd.xldate_as_tuple(cell_value, self.worksheet.book.datemode)
                try:
                    cell_value = datetime.datetime(*date_tuple)
                except:
                    return #encountered date 1/01/1900 in subject 8P35 line 516 which throws an exception here
            value_dict = {"value":cell_value}
            if self.data_units_xls.has_key(c):
                value_dict["units"] = self.data_units_xls[c]
            data_row_dict[sanitize_name(key)] = c== 0 and cell_value or value_dict #date needs to go in raw

    def extract_drug_info(self, data_dict, cell_value):
        drugs_dict = {}
        sentences = re.split("\.\\s+", cell_value)
        for sentence in sentences:
            rapa_match = re.search(rapa_re, sentence) or re.search(rapa_alt_re, sentence)
            fk_match = re.search(fk506_re, sentence) or re.search(fk506_alternate_re, sentence)
            
            
            if rapa_match:
                level = self.recover_level_of_drug(rapa_match.group(1))
                dose_time = rapa_match.group(2) == "BID" and ["AM","PM"] or [rapa_match.group(2)] # if BID replace with AM and PM
                [drugs_dict.__setitem__(sanitize_name("Rapamycin @ " + level + " " + dose + " Dose"), {"value": True}) for dose in dose_time]
                rapa_match2 = re.search(second_dose_re, sentence) or re.search(second_dose_alt_re, sentence)
                if rapa_match2:
                    drugs_dict[sanitize_name("Rapamycin @ " + level + " " + rapa_match2.group(2) + " Dose")] = {"value": True}
            elif fk_match:
                level = self.recover_level_of_drug(fk_match.group(1))
                dose_time = fk_match.group(2) == "BID" and ["AM","PM"] or [fk_match.group(2)]
                [drugs_dict.__setitem__(sanitize_name("FK506 @ " + level + " " + dose + " Dose"), {"value": True}) for dose in dose_time]
                fk_match2 = re.search(second_dose_re, sentence) or re.search(second_dose_alt_re, sentence)
                if fk_match2:
                    drugs_dict[sanitize_name("FK506 @ " + level + " " + fk_match2.group(2) + " Dose")] = {"value": True}
                                           
            #removed other drug information, getting too many attributes with true/false values
            
            elif "administered" in sentence.lower():
                m1 =  re.search(drug_re1, sentence)
                m2 = re.search(drug_re2, sentence)

                m3 = re.search(drug_re3, sentence)
                m4 = re.search(drug_re4, sentence)
                m = m1 or m2 or m3 or m4
                if m:
                    try:
                        dosage = float(m.group(1))
                        drugs_dict[sanitize_name(m.group(2))] = dosage
                        #data_dict[sanitize_name(m.group(2))] = dosage
                        continue
                    except:
                        drugs_dict[sanitize_name(m.group(1) + " @ " + m.group(2))] =  {"value": True}
                        
        if len(drugs_dict) > 0:        
            data_dict["Drugs Administered"] = drugs_dict
        
    def recover_level_of_drug(self, phrase):
        level_match = re.search(level_re, phrase)
        if level_match:
            return level_match.group(1) + " " + level_match.group(3)
        return phrase

    def unpack_insulin_values(self, data_dict, key, value):
        if len(key) == 2:
            insulin = re.search(insulin_re, value)
            if insulin:
                #some values are strings that cannot be converted to floats, drop these
                try:
                    data_dict[sanitize_name(key[0])] = {"value": float(insulin.group(1))}
                    data_dict[sanitize_name(key[1])] = {"value": insulin.group(2)}
                except:
                    pass
            return
        
        insulin = re.findall(insulin_re, value)
        if insulin:
            for (units, type_insulin) in insulin:
                if not units or not type_insulin:
                    continue
                dict_idx = {'R':0,'L':2,'N':4}[type_insulin]
                data_dict[sanitize_name(key[dict_idx])] = {"value":float(units)}
                data_dict[sanitize_name(key[dict_idx+1])] = {"value":type_insulin}


map_xls = { "fasting blood glucose" : "morning fasting blood glucose ",
            "post-prandial blood glucose" : "afternoon post-prandial blood glucose",
            "2 hour post-prandial blood glucose" : "afternoon post-prandial blood glucose",
            "units regular insulin:  pre-breakfast" : ["units pre-breakfast insulin","type pre-breakfast insulin"],
            "units regular insulin:  pre-lunch" : ["units short acting, pre- afternoon meal  insulin","type short acting, pre- afternoon meal  insulin",
                                                        "units long acting, pre- afternoon meal  insulin","type long acting, pre- afternoon meal  insulin",
                                                        "units normal acting, pre- afternoon meal  insulin","type normal acting, pre- afternoon meal  insulin"],
            "units regular insulin:  pre-luch" : ["units short acting, pre- afternoon meal  insulin","type short acting, pre- afternoon meal  insulin",
                                                        "units long acting, pre- afternoon meal  insulin","type long acting, pre- afternoon meal  insulin",
                                                        "units normal acting, pre- afternoon meal  insulin","type normal acting, pre- afternoon meal  insulin"],
            "RIA test Glucose" : "RIA test glucose",
            "Rapamune levels" : "Rapa level",
            "FK levels" : "FK506 level"
                }

