import sys
import xlrd

#add the directory above this to the paths searched common files used by various extractors
if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from db_helper import *
from parse_sheet_xls import *

def main(argv):
    #file = "SingleSheet.xls"
    #file = "dailymonitor-fixed.xls"
    filename = len(argv) > 0 and argv[0] or "dailymonitor-fixed.xls"
    
    run_extraction(filename)

def run_extraction(filename):
    db_x = db_helper()

    wb = xlrd.open_workbook(filename)

    #if the workbook contains the sheet Cumulative Values(Islet Isolation),
    if "template" not in wb.sheet_by_index(0).name:
        print "not going to extract file, not of daily monitor format."
        return

    current_category = ''
    for sheetx in xrange(0, wb.nsheets):
        ws = wb.sheet_by_index(sheetx)
        print ws.name
        if ws.name == "template":
            continue
        sheet = parse_sheet_xls(ws, current_category)

        if sheet.category != current_category:
            current_category = sheet.category
        else:
            db_x.insert_subject(sheet.subject_dict)         
            db_x.insert_timeseries(sheet.data)


if __name__ == "__main__":
    main(sys.argv[1:])
