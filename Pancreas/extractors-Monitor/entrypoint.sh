#!/bin/bash
set -e

# rabbitmq
if [ "$RABBITMQ_URI" == "" ]; then
    # configure RABBITMQ_URI if started using docker-compose or --link flag
    if [ -n "$RABBITMQ_PORT_5672_TCP_ADDR" ]; then
        RABBITMQ_URI="amqp://guest:guest@${RABBITMQ_PORT_5672_TCP_ADDR}:${RABBITMQ_PORT_5672_TCP_PORT}/${RABBITMQ_VHOST}"
    fi

    # configure RABBITMQ_URI if rabbitmq is up for kubernetes
    # TODO needs implementation maybe from NDS people
fi

#mongo, no document specified here
if [ "$MONGO_URI" == "" ]; then
    if [ -n "$MONGO_PORT_27017_TCP_PORT" ]; then
        MONGO_URI="mongodb://${MONGO_PORT_27017_TCP_ADDR}:${MONGO_PORT_27017_TCP_PORT}"
    fi
fi


# start server if asked
if [ "$1" = 'extractor' ]; then
    cd /home/clowder

    if [ "$RABBITMQ_PORT_5672_TCP_ADDR" != "" ]; then
        # start extractor after rabbitmq is up
        for i in `seq 1 10`; do
            if nc -z $RABBITMQ_PORT_5672_TCP_ADDR $RABBITMQ_PORT_5672_TCP_PORT ; then
                exec ./${MAIN_SCRIPT}
            fi
            sleep 1
        done
    fi

    # just launch extractor and see what happens
    exec ./${MAIN_SCRIPT}
fi

exec "$@"