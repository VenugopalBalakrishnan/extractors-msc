import sys
from openpyxl import load_workbook

from db_helper import *
from parse_sheet_optimized import *

def main(argv):
    file = len(argv) > 0 and argv[0] or  "modified P01 monitor 5 2  13.xlsx"
    run_extraction(file)

def run_extraction(file):
    db_x = db_helper()
    wb = load_workbook(file, use_iterators = True)
    current_category = ''
    timeseries_json = []
    subject_json = []
    

    #TODO: any better way of sensing type of excel file?
    if not wb.get_sheet_by_name("formula lists"):
        print "Not going to extract, not of P01 monitor type "
        return

    for ws in wb.worksheets:
        if ws.title == "formula lists":
            continue
        sheet = parse_sheet(ws, current_category)
        #print ws.title
        #print sheet.subject_dict
        if sheet.category != current_category:
            current_category = sheet.category
        else:
            
            db_x.insert_subject(sheet.subject_dict)
            db_x.insert_timeseries(sheet.data)
            
            # replace all datetime.datetime format as string. This is not done 
            # in parse_sheet since we don't want to change stuff store in DB, which 
            # is more important than json. 
            subject_dict = replaceDatetime(sheet.subject_dict, "msc")  
            subject_json.append({"msc:subject":subject_dict})
            timeseries_dict = replaceDatetime(sheet.data, "msc")
            timeseries_json.append({"msc:data":timeseries_dict})

    return subject_json, timeseries_json

if __name__ == "__main__":
    main(sys.argv[1:])
