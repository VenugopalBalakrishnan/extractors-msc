import re
from utilities import *

units_re =  re.compile("\w+?\\s*(\(\\w+?\/?\\w*?\))", re.IGNORECASE) #units are gotten as (mg/kg), makes extracting rest of name easier
units_alt_re = re.compile("\w+?\\s*(\\w+?\/{1}\\w*)\Z", re.IGNORECASE) #units are gotten as mg/kg only at end of string

class parse_sheet:

    def __init__(self, ws, current_category):
        self.data = []
        self.info_dict = {}
        self.headers = {}
        self.data_headers = {}
        self.data_units = {}
        self.category = current_category

        self.worksheet = ws

        self.name = ws.title
        reached_data = False
        reached_data_info_header = False

        for row in self.worksheet.iter_rows():
            if row[0][0] == 1:
                self.parse_subject_info_header(row)
            elif reached_data and reached_data_info_header:
                self.parse_data(row)
            elif reached_data_info_header:
                reached_data = self.parse_data_info_header(row)
            else:
                reached_data_info_header = self.parse_subject_info(row)

        if not reached_data and not reached_data_info_header:
            self.category = self.name

    def parse_subject_info_header(self, row):
        for cell in row: 
            name = cell.internal_value
            self.headers[cell.column] = name #all columns get a header, but only headers that contain text get a dictionary
            if name is not None:
                self.info_dict[name] = {}

    def parse_subject_info(self, row):
        for cell in row:
            cell_value = cell.internal_value
            col = cell.column

            #which of these is a better test of having reached data?
            if col == 'A' and not cell_value:
            #if cell_value and str(cell_value).strip()== "Insulin management":
                self.create_dict_for_db()
                return True
            elif col == 'A' and cell_value:
                key = cell_value
                key = key == "ID#" and "ID" or key
                continue
            elif not cell_value:
                continue

            if self.headers[col] and key and cell_value:
                self.info_dict[self.headers[col]][key] = cell_value

        return False

    def create_dict_for_db(self):
        #put the dicts in the order we want
        self.subject_dict = self.info_dict["Recipient"]
        self.subject_dict["ID"] = self.name
        self.subject_dict["Category"] = self.category

        for key in self.info_dict:
            if key != "Recipient" and len(self.info_dict[key]) > 0:
                self.subject_dict[key] = self.info_dict[key]


    def parse_data_info_header(self, row):
        for cell in row:
            name = cell.internal_value
            if cell.column == 'A' and not name:
                return False
            if name:
                units_match = re.search(units_re, name)
                if "@" not in name and units_match:
                    units = units_match.group(1)
                    self.data_units[cell.column] = units[1:-1]
                    name = name.replace(units, "").strip()
                elif "@" not in name:
                    units_match = re.search(units_alt_re, name)
                    if units_match:
                        units = units_match.group(1)
                        self.data_units[cell.column] = units
                        name = name.replace(units, "").strip()
                name = sanitize_name(name)

            self.data_headers[cell.column] = name #all columns get a header, some columns have None as name
        return True


    def parse_data(self, row):
        data_row_dict = {"ID":self.name, "Category":self.category}
        #FBG and PPG must exist, researchers tend to add many rows at end of sheet with valid dates, POD empty data
        necessary_keys = ["ID", "Date", "Category", self.data_headers["C"], self.data_headers["D"]]

        for cell in row:
            cell_value = cell.internal_value
            col = cell.column

            cell_value = sanitize_value(cell_value)

            if self.data_headers[col] and cell_value is not None:
                value_dict = {"value":cell_value}
                if col in self.data_units:
                    value_dict["units"] = self.data_units[col]
                data_row_dict[self.data_headers[col]] = self.data_headers[col] == "Date" and cell_value or value_dict #Date needs to go in raw
        
        if all(k in data_row_dict for k in necessary_keys) and len(data_row_dict) > len(necessary_keys):
            self.data.append(data_row_dict)





