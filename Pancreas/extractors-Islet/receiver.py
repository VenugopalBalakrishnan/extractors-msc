#!/usr/bin/env python
import pika
import requests
import sys
import logging
import time
import json
import subprocess
import tempfile
import os


if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import excel_load_islet_isolation
# ----------------------------------------------------------------------
# BEGIN CONFIGURATION
# ----------------------------------------------------------------------

# name where rabbitmq is running
rabbitmqhost = 'localhost'

# name to show in rabbitmq queue list
exchange = 'medici'

# name to show in rabbitmq queue list
extractorName = 'ncsa.msc.islet'

# username and password to connect to rabbitmq
username = None
password = None

# accept any type of file that is text
messageType = ['*.file.application.vnd_openxmlformats-officedocument_spreadsheetml_sheet.#']

# trust certificates, set this to false for self signed certificates
sslVerify=False

# ----------------------------------------------------------------------
# END CONFIGURATION
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup connection to server and wait for messages
def connect_message_bus():
    """Connect to message bus and wait for messages"""
    global extractorName, username, password, messageType, exchange

    # connect to rabbitmq using input username and password
    if (username is None or password is None):
        connection = pika.BlockingConnection()
    else:
        credentials = pika.PlainCredentials(username, password)
        parameters = pika.ConnectionParameters(host=rabbitmqhost, credentials=credentials)
        connection = pika.BlockingConnection(parameters)
    
    # connect to channel
    channel = connection.channel()
    
    # declare the exchange in case it does not exist
    channel.exchange_declare(exchange=exchange, exchange_type='topic', durable=True)
    
    # declare the queue in case it does not exist
    channel.queue_declare(queue=extractorName, durable=True)

    # connect queue and exchange
    [channel.queue_bind(queue=extractorName, exchange=exchange, routing_key=x) for x in messageType]

    # setting prefetch count to 1 so we only take 1 message of the bus at a time, so other extractors of the same type can take the next message.
    channel.basic_qos(prefetch_count=1)

    # start listening
    logger.info("Waiting for messages. To exit press CTRL+C")

    # create listener
    channel.basic_consume(on_message, queue=extractorName, no_ack=False)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    # close connection
    connection.close()
 
# ----------------------------------------------------------------------
# Process any incoming message
def on_message(channel, method, header, body):
    """When message is received do the following steps:
    1. download the file
    2. launch extractor function"""

    global logger, extractorName

    inputfile=None
    fileid=0

    try:
        # parse body back from json
        jbody=json.loads(body)
        host=jbody['host']
        fileid=jbody['id']
        secretKey=jbody['secretKey']
        intermediatefileid=jbody['intermediateId']
        secretKey=jbody['secretKey']
        if not (host.endswith('/')):
            host += '/'
        
         # tell everybody we are starting to process the file
        status_update(channel, header, fileid, "Started processing file")

        # download file
        inputfile = download_file(channel, header, host, secretKey, fileid, intermediatefileid)

        # call actual extractor function
        process_file(channel, header, host, secretKey, fileid, intermediatefileid, inputfile)
 

    except subprocess.CalledProcessError as e:
        msg = str.format("Error processing [exit code=%d]\n%s", e.returncode, e.output)
        logger.exception("[%s] %s", fileid, msg)
        status_update(channel, header, fileid, msg)
    except:
        logger.exception("[%s] error processing", fileid)
        status_update(channel, header, fileid, "Error processing")
    finally:
        status_update(channel, header, fileid, "Done")
        if inputfile is not None:
            try:
                os.remove(inputfile)
            except OSError:
                pass
            except UnboundLocalError:
                pass

        # notify rabbitMQ we are done processsing message
        channel.basic_ack(method.delivery_tag)


# ----------------------------------------------------------------------
# Send updates about status of processing file
def status_update(channel, header, fileid, status):
    """Send notification on message bus with update"""

    global extractorName, exchange

    logger.debug("[%s] : %s", fileid, status)

    statusreport = {}
    statusreport['file_id'] = fileid
    statusreport['extractor_id'] = extractorName
    statusreport['status'] = status
    statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
    channel.basic_publish(exchange=exchange,
                          routing_key=header.reply_to,
                          properties=pika.BasicProperties(correlation_id = header.correlation_id),
                          body=json.dumps(statusreport))

# ----------------------------------------------------------------------
# Download file from medici
def download_file(channel, header, host, key, fileid, intermediatefileid):
    """Download file to be processed from Medici"""

    global sslVerify

    status_update(channel, header, fileid, "Downloading file.")

    # fetch data
    url=host + 'api/files/' + intermediatefileid + '?key=' + key
    r=requests.get('%sapi/files/%s?key=%s' % (host, intermediatefileid, key),
                   stream=True, verify=sslVerify)
    r.raise_for_status()
    (fd, inputfile)=tempfile.mkstemp()
    with os.fdopen(fd, "w") as f:
        for chunk in r.iter_content(chunk_size=10*1024):
            f.write(chunk)
    return inputfile


# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(channel, header, host, key, fileid, intermediatefileid, inputfile):
    """Count the number of words in text file"""

    global sslVerify

    status_update(channel, header, fileid, "Extracting islet isolation data.")

    # call actual program
    excel_load_islet_isolation.run_extraction(inputfile)


if __name__ == '__main__':
    # configure the logging system
    logging.basicConfig(format="%(asctime)-15s %(name)-10s %(levelname)-7s : %(message)s",
                        level=logging.WARN)
    logger = logging.getLogger(extractorName)
    logger.setLevel(logging.DEBUG)

    # connect and process data    
    sys.exit(connect_message_bus())