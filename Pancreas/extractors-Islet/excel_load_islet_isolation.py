import sys
import xlrd
import json
from bson import json_util

from utilities import *
from db_helper import *

def main(argv):
    file = len(argv) > 0 and argv[0] or  "Cumulative Islet Isolation Parameter Data Values140922.xlsx"
    run_extraction(file)

def run_extraction(file):
    db_x = db_helper()
    wb = xlrd.open_workbook(file, encoding_override = "utf-8")
    donor_json = []


    ws = wb.sheet_by_index(0) #only reading first sheet (Master list). Confirm with Alex.
    if "Cumulative Values" not in ws.name:
        print "Not going to extract, not of Islet Isolation type"
        return
    sheet = islet_isolation_sheet(ws)
    sheet.parse_sheet()
    
    for donor_dict in sheet.data:
        if donor_dict["ID"] in sheet.disallowed_donors:
            continue
        db_x.insert_islet_isolation(donor_dict)

        donor_dict = replaceDatetime(donor_dict, "msc")  
        donor_json.append({"msc:donor":donor_dict})
    return donor_json

class islet_isolation_sheet:
    def  __init__(self, ws):
        self.ws = ws;
        self.data_headers = {}
        self.reached_data = False
        self.data = []
        self.islet_donors_set = set()
        self.disallowed_donors = set()

    def parse_sheet(self):
        for r in range(0,self.ws.nrows):
            if not self.reached_data:
                self.parse_info_header(r)
            else:
                self.parse_data_row(r)

    def parse_info_header(self, r):
        row = self.ws.row(r)
        for col in range(self.ws.ncols):
            cell = row[col]
            name = cell.value
            if name == "NHP Number:":
                self.reached_data = True
                
            elif not self.reached_data:
                return
          
            if name:
                name = sanitize_name(name)
                name.encode('utf-8')
            self.data_headers[col] = name #all columns get a header
        #print self.data_headers

    def parse_data_row(self, r):
        row = self.ws.row(r)
        data_row_dict = {}
        islet_donor = None
        for col in range(self.ws.ncols):
            cell = row[col]
            cell_value = cell.value

            cell_value = sanitize_value(cell_value)

            #if the cell value was None, -99, -77 or blank, keep going
            if cell_value is None or cell_value == -99 or cell_value == -77 or cell_value == "" or cell_value == "Pending": 
                continue

            if "NHP ID Number" in self.data_headers.get(col) or "Recipient Cyno ID" in self.data_headers.get(col):
                cell_value = sanitize_name(str(cell_value))
            
            if "NHP ID Number" in self.data_headers.get(col):  #xlrd.colname(col) == "C":
                islet_donor = cell_value
                if any(x in islet_donor for x in [",","&"]): #cant deal with multiple donors on one line
                    return
                #if donor already exists in added donors, dont add this info, also put the donor on list of donors to be removed
                if islet_donor in self.islet_donors_set:
                    self.disallowed_donors.add(islet_donor)
                    return
                self.islet_donors_set.add(islet_donor)
            elif self.data_headers[col]:
                data_row_dict[self.data_headers[col]] = {"value":cell_value}

        if islet_donor:
            self.data.append({"ID": islet_donor, "data":data_row_dict})

if __name__ == "__main__":
    main(sys.argv[1:])
