# =============================================================================
#
# In order for this extractor to run according to your preferences,
# the following parameters need to be set.
#
# Some parameters can be left with the default values provided here - in that
# case it is important to verify that the default value is appropriate to
# your system. It is especially important to verify that paths to files and
# software applications are valid in your system.
#
# =============================================================================

import os

# name to show in rabbitmq queue list
extractorName = os.getenv('RABBITMQ_QUEUE', "ncsa.msc.PhenotypeExtractor" )

# URL to be used for connecting to rabbitmq
rabbitmqURL = os.getenv('RABBITMQ_URI', "amqp://guest:guest@127.0.0.1:5672/%2f")

# name of rabbitmq exchange
rabbitmqExchange = os.getenv('RABBITMQ_EXCHANGE', "clowder")

# type of files to process
messageType = ['*.file.application.vnd_ms-excel_sheet_macroenabled_12', 
                '*.file.application.vnd_openxmlformats-officedocument_spreadsheetml_sheet', 
                "extractors."+extractorName]

# trust certificates, set this to false for self signed certificates
sslVerify = os.getenv('RABBITMQ_SSLVERIFY', False)

# Endpoints and keys for registering extractor information in CSV format.
registrationEndpoints = os.getenv('REGISTRATION_ENDPOINTS', "http://localhost:9000/clowder/api/extractors?key=r1ek3rs")

#mongo URL for storing msc data point
mongoURL = os.getenv('MONGO_URI', "mongodb://127.0.0.1:27017")

#user and password for document "msc"
db_username = os.getenv('DB_USERNAME', "")
db_password = os.getenv('DB_PASSWORD', "")
