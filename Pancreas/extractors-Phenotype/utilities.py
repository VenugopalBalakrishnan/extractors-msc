import re
from datetime import datetime
from bson import ObjectId

def sanitize_name(name):
    name = name.strip("%")
    name = name.strip()
    name = re.sub('[/\."*<>:|?]','_', name)
    return name

def sanitize_value(value):
    if value == u"\u25cf" or value == u'\u002B':
        return True
    elif value == '#DIV/0!' or value == "#N/A":
        return None
    return value

def replaceDatetime(sheetdata, extracorName): 
    """ replace all the Datatime in dict and list"""   
    if isinstance(sheetdata, list):
        for n, temp in enumerate(sheetdata):
            sheetdata[n] = replaceDatetime(temp, extracorName)
        return sheetdata

    elif isinstance(sheetdata, dict):
        output = {}
        for k, v in sheetdata.items():
            output[extracorName +":"+str(k)] = replaceDatetime(sheetdata[k], extracorName)
        return output             
    else:
        if isinstance(sheetdata, datetime) or isinstance(sheetdata, ObjectId):
            return str(sheetdata)
        else:
            return sheetdata
