import sys
import xlrd
import datetime

from db_helper import *
from utilities import *

skippable_cols = ['Data Unit', 'Animal #','Sampling Interval']
nonprepend_cols = ['Data Unit', 'MSC', 'MSC origen', 'Number of MSC infusion', 'Sampling Date', 'POD', 'WBC (x103/ml)']
name_prepend_map = {"Summary in %" : "Percent ", "Summary in abs ct" : "Abs Count ", "% baseline abs" : "Percent change ", "% change baseline" : "Percent change "}
map_col_names = {'Sampling Date' : 'Date', 'MSC origen' : 'MSC origin'}

def main(argv):
    filename = len(argv) > 0 and argv[0] or "P01 Phenotype.xlsx"
    run_extraction(filename)

def run_extraction(filename):
    db_x = db_helper()
    timeseries_json = []
    subject_json = []

    wb = xlrd.open_workbook(filename)
    # process the file only if the sheet name contains any of name_prepend_map's keys
    if not any(k in wb.sheet_by_index(0).name for k in name_prepend_map.keys()) :
        print "not going to extract file, does not seem to be phenotype data"
        return

    for sheetx in xrange(0, wb.nsheets, 3):
        sheet = animal_reader(wb, sheetx)

        db_x.insert_subject(sheet.subject_dict)         
        db_x.insert_timeseries(sheet.data)

        # replace all datetime.datetime format as string. This is not done 
        # in parse_sheet since we don't want to change stuff store in DB, which 
        # is more important than json. 
        subject_dict = replaceDatetime(sheet.subject_dict, "msc")  
        subject_json.append({"msc:subject":subject_dict})

        timeseries_dict = replaceDatetime(sheet.data_header, "msc")
        timeseries_json.append({"msc:data":timeseries_dict})

    return subject_json, timeseries_json

#class to read standard phenotype file from Miami
class animal_reader:
    def __init__(self, wb, isheet):
        self.subject_dict = {}
        self.data_header = {}
        self.data = []

        self.workbook = wb
        self.name = self.workbook.sheet_by_index(isheet).name.split()[1]
        self.subject_dict["ID"] = self.name

        [self.read_sheet(i, name_prepend_map[k]) for i in range(isheet, isheet+3) for k in name_prepend_map.keys() if k in wb.sheet_by_index(i).name]

    def read_sheet(self, index, prepend_header_with):
        self.worksheet = self.workbook.sheet_by_index(index)

        self.parse_data_header(0, prepend_header_with)
        until_col=4
        self.parse_time_independent_data(1, until_col)
        self.parse_data(start_row=1, start_col=until_col+1)

    def parse_data_header(self, row, prepend_txt):
        for col in range(self.worksheet.ncols):
            cell = self.worksheet.cell(row, col)
            header = cell.value.strip()
            if header not in nonprepend_cols or (prepend_txt == name_prepend_map["% baseline abs"] and header == 'WBC (x103/ml)'):
                header = prepend_txt + header
            self.data_header[col] = header
        print self.data_header

    def parse_time_independent_data(self, row, until_col):
        for col in range(0, until_col):
            cell = self.worksheet.cell(row, col)
            self.process_value_for_dict(self.subject_dict, cell, col, True)
            #process the special 'Data Unit' that we want skipped in subject_dict, but want for the data
            if self.data_header.get(col) == 'Data Unit':
                self.data_units = sanitize_value(cell.value)

    def parse_data(self, start_row, start_col):
        necessary_keys = ["ID", "Date", "POD"]
        invalid_cell_types = [xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_ERROR, xlrd.XL_CELL_BLANK]

        for row in range(start_row, self.worksheet.nrows):
            data_row_dict = {"ID":self.name}

            for col in range(start_col, self.worksheet.ncols):
                cell = self.worksheet.cell(row, col)
                if any(x == cell.ctype for x in invalid_cell_types):
                    continue

                self.process_value_for_dict(data_row_dict, cell, col)
            
            if all(k in data_row_dict for k in necessary_keys) and len(data_row_dict) > len(necessary_keys):
                self.data.append(data_row_dict)

    def process_value_for_dict(self, data_row_dict, cell, col, insert_raw=False):
        if not self.data_header.has_key(col):
            return

        cell_value = sanitize_value(cell.value)
        if cell_value == None or cell_value == '': # cell_value==None wont eliminate cell_value = 0 values
            return

        key = self.data_header[col]
        if any(x in key for x in skippable_cols):
            return
        key = map_col_names.get(key, key)

        if key == 'Date' and not isinstance(cell_value, float): # getting some unreadable unicode char
            return
        elif key == 'Date': #Date needs processing from xlsx files
            date_tuple = xlrd.xldate_as_tuple(cell_value, self.worksheet.book.datemode)
            try:
                cell_value = datetime.datetime(*date_tuple)
            except:
                return #guard against dates like 1/01/1900
        
        sanitized_key = sanitize_name(key)
        if insert_raw or key == 'Date':
            data_row_dict[sanitized_key] = cell_value
        else:
            data_row_dict[sanitized_key] = {"value":cell_value}
            if hasattr(self, 'data_units') and key not in nonprepend_cols:
                data_row_dict[sanitized_key]["units"] = self.data_units

if __name__ == "__main__":
    main(sys.argv[1:])
