#!/usr/bin/env python

import sys
import json
import requests

#add the directory above this to the paths searched common files used by various extractors
if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


import logging
from config import *
import pyclowder.extractors as extractors
from excel_load_phenotype import run_extraction

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger

    # set logging
    logging.basicConfig(format='%(asctime)-15s %(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)
    logger = logging.getLogger('extractor')
    logger.setLevel(logging.DEBUG)
    register_extractor(registrationEndpoints)

    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   rabbitmqURL=rabbitmqURL,
                                   rabbitmqExchange=rabbitmqExchange,
                                   processFileFunction=process_file,
                                   checkMessageFunction=None)

def process_file(parameters):
    ''' Process the file and upload the results
    :param parameters:
    :return:
    '''
    global extractorName

    # get file's content
    inputfile = parameters['inputfile']
    [subject_json, timeseries_json] = run_extraction(inputfile)

    # context url
    context_url = 'https://clowder.ncsa.illinois.edu/contexts/metadata.jsonld'

    # store results as metadata
    metadata = {
                '@context': [context_url, 
                    {"msc": "http://clowder.ncsa.illinois.edu/clowder/api/extractors/ncsa.msc#",
                    }
                ],
                'attachedTo': {'resourceType': 'file', 'id': parameters["fileid"]},
                'agent': {
                    '@type': 'cat:extractor',
                    'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/ncsa.msc.PhenotypeExtractor'
                },
                'content': {'msc:subjects' : subject_json,
                            'msc:timeseries' : timeseries_json}
                }


    # upload metadata
    extractors.upload_file_metadata(mdata=metadata, parameters=parameters)

    logger.info("Uploaded metadata %s", metadata)

def register_extractor(registrationEndpoints):
    """Register extractor info with Clowder"""

    logger.info("Registering extractor")

    headers = {'Content-Type': 'application/json'}

    try:
        with open('extractor_info.json') as info_file:
            info = json.load(info_file)
            # This makes it consistent: we only need to change the name at one place: config.py.
            info["name"] = extractorName
            for url in registrationEndpoints.split(','):
                # Trim the URL in case there are spaces in the config.py string.
                r = requests.post(url.strip(), headers=headers, data=json.dumps(info), verify=sslVerify)
                logger.debug("Registering extractor with " +  url + " : " + r.text)
    except Exception as e:
        logger.error('Error in registering extractor: ' + str(e))


if __name__ == '__main__':
    main()
