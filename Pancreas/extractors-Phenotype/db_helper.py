import pymongo
import re

from config import *

def restructure_for_msc_webapp(data_dict):
    restructured_dict = {}
    for key in ["Category", "Date"]:
        if data_dict.has_key(key):
            restructured_dict[key] = data_dict.pop(key)
    restructured_dict["subject"] = data_dict.pop("ID")

    subvalue_dict = {}
    for k,v in data_dict.items():
        subvalue_dict[ re.sub(' +', ' ', k)] = v
    restructured_dict["data"] = subvalue_dict
    return restructured_dict

class db_helper:
    def __init__(self):
        try:
            if mongoURL:
                print mongoURL
                client = pymongo.MongoClient(mongoURL)
            else:
                client = pymongo.MongoClient()
        except pymongo.errors.ConnectionFailure:
            print "Could not connect to the Mongo client {}".format(mongoURL)
            
        db = client.msc
        if db_username and db_password:
            db.authenticate(db_username, db_password)

        self.db = db

    def insert_timeseries(self, sheet_data):
        collection = self.db.timeseries

        for data_dict in sheet_data:
            data_dict = restructure_for_msc_webapp(data_dict)
           
            try:
                existing_records = collection.find({"subject":data_dict["subject"], "Date":data_dict["Date"]})
            except:
                print data_dict

            if existing_records.count() > 1:
                print "WE HAVE TOO MANY DUPLICATE RECORDS FOR ", data_dict["subject"], data_dict["Date"]
            elif existing_records.count() == 1 and data_dict.has_key('data'): #if record exists, update the "data" dict in it
                old_data = collection.find_one({"subject":data_dict["subject"], "Date":data_dict["Date"]})
                for k,v in old_data["data"].items():
                    if not data_dict["data"].has_key(k):
                        data_dict["data"][k] = v
                collection.update({"subject":data_dict["subject"], "Date":data_dict["Date"]}, {"$set": data_dict}, upsert=False)
            
            
            else:
                collection.insert(data_dict)

    def insert_subject(self, subject):
        collection = self.db.patients
        if not collection.find_one() or collection.find_one() and not collection.find_one({"ID": subject["ID"]}):
            collection.insert(subject)
            print "Inserted subject {0}".format(subject["ID"])
        elif collection.find_one({"ID": subject["ID"]}): #update the record if it already exists
            collection.update({"ID":subject["ID"]}, {"$set": subject}, upsert=False)

    #currently does NOT update existing collection.
    def insert_islet_isolation(self, donor_dict):
        collection = self.db.islet_donors
        if not collection.find_one() or collection.find_one() and not collection.find_one({"ID": donor_dict["ID"]}):
            collection.insert(donor_dict)
            print "Inserted islet donor {0}".format(donor_dict["ID"])
