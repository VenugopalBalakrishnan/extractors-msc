import re



def sanitize_name(name):
    name = name.strip("%")
    name = name.strip()
    name = re.sub('[/\."*<>:|?]','_', name)
    return name

def sanitize_value(value):
    if value == u"\u25cf" or value == u'\u002B':
        return True
    elif value == '#DIV/0!' or value == "#N/A":
        return None
    return value

