import sys
import pymongo
from pymongo import MongoClient
from db_access import *

def main(argv):
    file = len(argv) > 0 and argv[0] 
    f = open(file, "r")
    run_extraction(f)

def run_extraction(f):
    attributename = ""
    a = f.readline().rstrip()
    b = f.readline().rstrip()
    attributename = dict({'data.' + a : 'data.' + b})

    print attributename
    try:
        client = MongoClient()
    except:
        print "Could not connect to the Mongo client"
        raise
    db = client.msc
    if db_username and db_password:
        db.authenticate(db_username, db_password)
    collection = db.timeseries
    collection.update({}, {"$rename": {attributename}}, mutli = True)
	
if __name__ == "__main__":
    main(sys.argv[1:])
