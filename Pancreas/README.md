# ncsa.msc extractors

## Dependencies


  * Using virtualenv  
    You may use the included requirements.txt file along with virtualenv to automatically install all required python libraries in a sandboxed python environment

        # Create a virtual environment  
        $ virtualenv -p=python2.7 ENV
        
        # Activate the virtual environment  
        $ source ENV/bin/activate
          
        # Install the required python modules  
        $ pip install -r requirements.txt        

    When you are finished using your virtual environment, be sure to run the deactivate script to restore your $PATH

        $ deactivate

  * System wide  
    Alternately, you may use the included requirements.txt file to install all required libraries system wide

        $ sudo pip install -r requirements

  * Individual packages  
    You may also install the individual dependencies listed below with pip using

        $ sudo pip install [package name]

  * **requests**  
[http://docs.python-requests.org/en/latest/](http://docs.python-requests.org/en/latest/)  

  * **pika**  
[https://pypi.python.org/pypi/pika](https://pypi.python.org/pypi/pika)  
       
  * **openpyxl**  
[https://pypi.python.org/pypi/openpyxl](https://pypi.python.org/pypi/openpyxl)  

  * **xlrd**  
[https://pypi.python.org/pypi/xlrd](https://pypi.python.org/pypi/xlrd)  

  * **pymongo**   
[http://api.mongodb.org/python/current/](http://api.mongodb.org/python/current/)  


## Usage

  * Download the latest data from server
    (You need to connect with staff in NCSA for approval)
    Currently, the number of *.xlsm files in each directory is:  
    ChemCBCExtractor   2  
    PhenotypeExtractor 1  
    MonitorExtractor   1  

  * Strat rabbitmq.

        $ rabbitmq-server

    default port 15672

  * Start clowder

    use jsonId version; default port 9000  
    delete comment on "9992:services.RabbitmqPlugin" in play.plugins  
    accordingly modify "RabbitMQ" in application.conf  

  * Start exreactor

        $ python XX/extractor.py

    check the extractor on rabbitmq --> queues

  * Upload files

    clowder --> Files --> upload  
    then click "List extraction events" under the file page to view log.

  * Check data

    We encourage to run the msc server locally and compare #Data points and #Patients on 
https://mscp01.ncsa.illinois.edu/msc/

## Run on Docker
    $ docker-compose up -d
    $ docker build -t msc-extractor .
    $ docker run --rm -i -t --link monitorextractor_rabbitmq_1:rabbitmq --link monitorextractor_mongo_1:mongo msc-extractor

change MONGO_URL, DB_USERNAME and DB_PASSWORD when connect to https://mscp01.ncsa.illinois.edu/msc/

To change clowder as private. We current has no better way other than go to Exec of clowder docker and
    
      $ bash
      $ cd /home/clowder/custom/
      $ echo 'permissions="private"' >> custom.conf

then restart. 



