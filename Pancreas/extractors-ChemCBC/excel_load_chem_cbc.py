import xlrd
import datetime

from db_helper import *
from utilities import *

def main(argv):
    filename = len(argv) > 0 and argv[0] or "Chemistry - PO1 for U of Illinois.xlsx"
    run_extraction(filename)

def run_extraction(filename):
    db_x = db_helper()

    wb = xlrd.open_workbook(filename)
    timeseries_json = []
    subject_json = []

    #TODO: find better way?
    # if the workbook first sheet is Cumulative Values(Islet Isolation), 
    # contains formula lists(p01 monitor data) or first sheet is template(dailymonitor), ignore
    if 'Cumulative Values' in wb.sheet_by_index(0).name or 'template' in wb.sheet_by_index(0).name:
        print "not going to extract file, not of Chem or CBC type "
        return

    for sheetx in xrange(0, wb.nsheets):
        ws = wb.sheet_by_index(sheetx)
        sheet = sheet_reader(ws)

        db_x.insert_subject(sheet.subject_dict)
        # replace all datetime.datetime format as string. This is not done 
        # in parse_sheet since we don't want to change stuff store in DB, which 
        # is more important than json. 
        subject_dict = replaceDatetime(sheet.subject_dict, "msc")  
        subject_json.append({"msc:subject":subject_dict})

        db_x.insert_timeseries(sheet.data)
        timeseries_dict = replaceDatetime(sheet.data_header, "msc")
        timeseries_json.append({"msc:data":timeseries_dict})

    return subject_json, timeseries_json

#class to read standard chem or cbc file from Miami
class sheet_reader:
    def __init__(self, ws):
        self.subject_dict = {}
        self.data_header = {}
        self.data_units = {}
        self.data = []

        self.worksheet = ws
        self.read_sheet()

    def read_sheet(self):
        self.name = sanitize_name(self.worksheet.cell(0,0).value)
        self.subject_dict["ID"] = self.name
         
        self.parse_data_header(row=1)
        self.parse_data_units(row=2)
        self.parse_data(start_row=3)

    def parse_data_header(self, row):
        for col in range(self.worksheet.ncols):
            cell = self.worksheet.cell(row, col)
            header = cell.value
            self.data_header[col] = header

    def parse_data_units(self, row):
        for col in range(self.worksheet.ncols):
            cell = self.worksheet.cell(row, col)
            units = cell.value
            if units == None or units == '': # cell_value==None wont eliminate cell_value = 0 values
                continue
            self.data_units[col] = units

    def parse_data(self, start_row):
        necessary_keys = ["ID", "Date", "POD"]
        invalid_cell_types = [xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_ERROR, xlrd.XL_CELL_BLANK]

        for row in range(start_row, self.worksheet.nrows):
            data_row_dict = {"ID":self.name}

            for col in range(self.worksheet.ncols):
                cell = self.worksheet.cell(row, col)
                if any(x == cell.ctype for x in invalid_cell_types):
                    continue

                self.process_value_for_dict(data_row_dict, cell, col)
            
            if all(k in data_row_dict for k in necessary_keys) and len(data_row_dict) > len(necessary_keys):
                self.data.append(data_row_dict)

    def process_value_for_dict(self, data_row_dict, cell, col):
        if not self.data_header.has_key(col):
            return

        cell_value = sanitize_value(cell.value)
        if cell_value == None or cell_value == '': # cell_value==None wont eliminate cell_value = 0 values
            return

        key = self.data_header[col]
        if col == 0 and not isinstance(cell_value, float): # getting some unreadable unicode char
            return
        elif col == 0: #Date needs processing from xlsx files
            date_tuple = xlrd.xldate_as_tuple(cell_value, self.worksheet.book.datemode)
            try:
                cell_value = datetime.datetime(*date_tuple)
            except:
                return #guard against dates like 1/01/1900
        
        value_dict = {"value":cell_value}
        if self.data_units.has_key(col):
            value_dict["units"] = sanitize_name(self.data_units[col])

        data_row_dict[sanitize_name(key)] = col == 0 and cell_value or value_dict #date needs to go in raw
                 

if __name__ == "__main__":
    main(sys.argv[1:])
