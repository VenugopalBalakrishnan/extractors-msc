function [out] = ii(im, SVMModelii, SVMModelii2nd, imglom)
nfeature =8;
slidesize = 50;
stepsize = 50;
n = slidesize* slidesize;

[height, weight, ~] = size(im);
x= int16((height-slidesize)/stepsize);
y = int16((weight-slidesize)/stepsize);
T = zeros(x*y,nfeature+2);
k=1;
for i =1: x
    for j = 1:y
        imtmp= imcrop(im, [j*stepsize-stepsize+1 i*stepsize-stepsize+1  slidesize-1 slidesize-1]);
        if numel(imtmp) == n *3 && nnz(imtmp) > numel(imtmp)* 0.8
            statsred= graycoprops(imtmp(:, :,1));
            
            statsblue= graycoprops(imtmp(:, :,3));
            T(k, 1)=j*stepsize-stepsize+1 + slidesize /2;
            T(k, 2) =i*stepsize-stepsize+1 + slidesize /2;
            T(k, 3:nfeature+2) = [statsred.Contrast  statsred.Correlation  statsred.Homogeneity  statsred.Energy ...
                statsblue.Contrast  statsblue.Correlation  statsblue.Homogeneity  statsblue.Energy];
            k=k+1;
        end
    end
end

T( all(~T,2), : ) = [];
total = size(T, 1);
v = predict(SVMModelii,T(:, 3:nfeature+2));
T( cell2mat(v) == 'H', :) = [];
pts = int32(T(:, 1:2));

for i =1: size(pts, 1)
    if nnz( imglom(pts(i, 2)-10:pts(i, 2)+10, pts(i, 1)-10: pts(i, 1)+10)) > 0
        pts(i, 2) =0;
        pts(i, 1) =0;
        %         else
        %           imwrite( imcrop(im, [Pts(i, 1)- 100 Pts(i, 2)-100 200-1 200-1]), strcat('tmp/', int2str(imn),'-', int2str(i) , '.tiff'));
    end
end
pts( pts(:,1) == 0, :) = [];
nfeature =6;
slidesize = 200;


level = 0.34;
level2 = 0.8;
sel = strel('line',1,90);
sel2 = strel('line',1,90);
[totalsample, ~] = size(pts);

T = zeros(totalsample,nfeature+2);

for i =1: totalsample
    
    imtmp= imcrop(im, [pts(i,1)-slidesize/2  pts(i, 2)-slidesize/2  slidesize-1 slidesize-1]);
    imglomtmp = imcrop(imglom, [pts(i,1)-slidesize/2  pts(i, 2)-slidesize/2  slidesize-1 slidesize-1]);
    GLCM2 = graycomatrix(rgb2gray(imtmp),'Offset',[2 0;0 2]);
    stats = GLCM_Features1(GLCM2,0);
    
    imtmp = immultiply(imtmp, repmat(~imglomtmp,[1 1 3]));
    bw = ~im2bw(imtmp,level);
    cell = bwpropfilt(bw, 'MajorAxisLength', [3, 30]);
    bw = im2bw(imtmp,level2);
    bw = imerode(bw, sel);
    bw = imerode(bw, sel2);
    white = bwpropfilt(bw, 'Area', [100, 100000]);
    
    T(i, 1)=pts(i,1);
    T(i, 2) =pts(i,2);
    T(i, 3:nfeature+2) = [nnz(cell)  nnz(white)  stats.inf2h(1) stats.cprom(1) stats.idmnc(1)  stats.maxpr(1)];
    
end

v = predict(SVMModelii2nd,T(:,3:nfeature+2));

T( cell2mat(v) == 'H', :) = [];
if size(T, 1) > total * 0.05
    out = size(T, 1);
else
    out = 0;
end

end
