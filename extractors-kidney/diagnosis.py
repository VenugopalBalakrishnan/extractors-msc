import sys
import subprocess
import os
import tempfile
import time
#add the directory above this to the paths searched common files used by various extractors
if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import logging
from config import *
import pyclowder.extractors as extractors

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger
    global matlab_process

    matlab_process = None

    # set logging
    logging.basicConfig(format='%(asctime)-15s %(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)
    logger = logging.getLogger('extractor')
    logger.setLevel(logging.DEBUG)
    register_extractor(registrationEndpoints)
    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   rabbitmqURL=rabbitmqURL,
                                   rabbitmqExchange=rabbitmqExchange,
                                   processFileFunction=process_file,
                                   checkMessageFunction=None)
    # kill matlab
    if matlab_process and not matlab_process.poll():
        matlab_process.stdin.write("quit()\n")
        matlab_process.kill()



def process_file(parameters):
    global extractorName
    
    inputfile=parameters['inputfile']
    tmpfile=inputfile+"-cad.txt"

    try:
        run_classify(inputfile, tmpfile)

        # tmpfile resulting from running the matlab code should contain two lines. 1st = category, 2nd = score
        mdata={}
        # mdata["extractor_id"]=extractorName
        f = open(tmpfile, 'r')
        ii = f.readline().strip('\n')
        mdata["Interstitial Inflammation"]=[ii]
        tc = f.readline().strip('\n')
        mdata["Tubular Cast"]=[tc]

        glom = tc
        glommeta=[]
        while(glom != ''):
        	glom = f.readline().strip('\n')
        	glommeta.append(glom)
        f.close()
        glommeta.pop()
        mdata['Glomerulus'] = glommeta
        print parameters
        # context url
        context_url = 'https://clowder.ncsa.illinois.edu/clowder/contexts/metadata.jsonld'
        context_glom ={
                       "@context":{
                                   "Glomerulus":{
                                                 "@id" :"http://ntp.niehs.nih.gov/nnl/urinary/kidney/glmeta/",
                                                 "@type": "http://www.w3.org/2001/XMLSchema#string"},
                                   "Tubular Cast":{
                                                   "@id" :"https://finto.fi/mesh/en/page/D012671",
                                                   "@type": "http://www.w3.org/2001/XMLSchema#string"},
                                   "Interstitial Inflammation":{
                                                                "@id" :"http://ntp.niehs.nih.gov/nnl/urinary/kidney/inflamm/",
                                                                "@type": "http://www.w3.org/2001/XMLSchema#string"}
                                 }
                    }
        metadata = {
                     '@context': [context_url, context_glom],                      
                     'attachedTo': {'resourceType': 'file', 'id': parameters["fileid"]},
                     'agent': {
                         '@type': 'cat:extractor',
                         'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/ncsa.msc.diagnosis'},
                      'content': mdata
                }
        from pprint import pprint
        pprint(metadata)
        extractors.upload_file_metadata(mdata=metadata, parameters=parameters)

    finally:
        if os.path.isfile(tmpfile):
            os.remove(tmpfile)


def run_classify(inputfile, outputfile):
    global matlab_process, matlabBinary

    logger.info(matlabBinary)
    if not matlab_process or matlab_process.poll():
        folder = os.path.dirname(os.path.realpath(__file__))
        args = [matlabBinary, '-nodisplay', '-nosplash']
        matlab_process = subprocess.Popen(args, stdin=subprocess.PIPE, shell=True)
        matlab_process.stdin.write("cd '" + folder + "';\n")

    matlab_process.stdin.write("load('SVMModelglom1.mat');\n")
#    matlab_process.stdin.write("load('SVMModelhog.mat');\n")
    matlab_process.stdin.write("load('SVMModelii.mat');\n")
    matlab_process.stdin.write("load('SVMModelii2nd.mat');\n")
    matlab_process.stdin.write("load('SVMModeltc.mat');\n")
    matlab_process.stdin.write("load('SVMModeltc2nd.mat');\n")
    matlab_process.stdin.write("imorigin = imread('" + inputfile + "');\n")
    matlab_process.stdin.write("[outGLOM, imglom] = glom(imorigin, SVMModelglom1);\n")
    matlab_process.stdin.write("[outII] = ii(imorigin, SVMModelii, SVMModelii2nd, imglom);\n")
    matlab_process.stdin.write("[outTC] = tc(imorigin, SVMModeltc, SVMModeltc2nd);\n")
    matlab_process.stdin.write("fileID = fopen('" + outputfile + "','w');\n")
    matlab_process.stdin.write("fprintf(fileID,'%d\\n', outII);\n")
    matlab_process.stdin.write("fprintf(fileID,'%d\\n', outTC);\n")
    matlab_process.stdin.write("fprintf(fileID,'%4.2f  %4.2f\\n', outGLOM);\n")
    matlab_process.stdin.write("fclose(fileID);\n")

    while not os.path.isfile(outputfile) and not matlab_process.poll():
        time.sleep(0.1)


def register_extractor(registrationEndpoints):
    """Register extractor info with Clowder"""

    logger.info("Registering extractor")

    headers = {'Content-Type': 'application/json'}

    try:
        with open('extractor_info.json') as info_file:
            info = json.load(info_file)
            # This makes it consistent: we only need to change the name at one place: config.py.
            info["name"] = extractorName
            for url in registrationEndpoints.split(','):
                # Trim the URL in case there are spaces in the config.py string.
                r = requests.post(url.strip(), headers=headers, data=json.dumps(info), verify=sslVerify)
                logger.debug("Registering extractor with " +  url + " : " + r.text)
    except Exception as e:
        logger.error('Error in registering extractor: ' + str(e))

if __name__ == "__main__":
    main()

