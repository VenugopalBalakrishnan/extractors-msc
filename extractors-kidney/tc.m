function[out] =  tc(im, SVMModeltc, SVMModeltc2nd)
nfeature =8;
slidesize = 50;
stepsize = 50;
n = slidesize * slidesize;

[height, weight, ~] = size(im);
x= int16((height-slidesize)/stepsize);
y = int16((weight-slidesize)/stepsize);
T = zeros(x*y,nfeature+2);
k=1;
for i =1: x
    for j = 1:y
        imtmp= imcrop(im, [j*stepsize-stepsize+1 i*stepsize-stepsize+1  slidesize-1 slidesize-1]);
        if nnz(imtmp) > numel(imtmp)-10
            statsred= graycoprops(imtmp(:, :,1));
            statsblue= graycoprops(imtmp(:, :,3));
            T(k, 1)=j*stepsize-stepsize+1 +slidesize /2;
            T(k, 2) =i*stepsize-stepsize+1 +slidesize /2;
            T(k, 3:nfeature+2) = [statsred.Contrast  statsred.Correlation  statsred.Homogeneity  statsred.Energy ...
                statsblue.Contrast  statsblue.Correlation  statsblue.Homogeneity  statsblue.Energy];
            k=k+1;
        end
    end
end

T( all(~T,2), : ) = [];
total = size(T, 1);
v = predict(SVMModeltc,T(:, 3:nfeature+2));
T( cell2mat(v) == 'H', :) = [];
pts = int32(T(:, 1:2));

[totalsample, ~] = size(pts);
nfeature =7;
T = zeros(totalsample,nfeature+2);

for i =1: totalsample
    
    imtmp= imcrop(im, [pts(i,1)-slidesize/2  pts(i, 2)-slidesize/2  slidesize-1 slidesize-1]);
    rfilt = rangefilt(imtmp(:, :,1), ones(7));
    rfilt = double(reshape(rfilt, n, 1));
    rfilt = median(rfilt) /40;
    
    bfilt = rangefilt(imtmp(:, :,3), ones(7));
    bfilt = double(reshape(bfilt, n, 1));
    bfilt = median(bfilt) /40;
    
    GLCM2 = graycomatrix(rgb2gray(imtmp) ,'Offset',[2 0;0 2]);
    stats = GLCM_Features1(GLCM2,0);
    
    hsv = rgb2hsv(imtmp);
    hmean = double(reshape(hsv(:, :, 1), n, 1));
    hmean = median(hmean);
    smean = double(reshape(hsv(:, :, 2), n, 1));
    smean = median(smean);
    T(i, 1)=pts(i,1);
    T(i, 2) =pts(i,2);
    T(i, 3:nfeature+2) = [rfilt bfilt stats.dvarh(1) stats.senth(1) stats.denth(1) hmean smean];
    
end

v = predict(SVMModeltc2nd,T(:,3:nfeature+2));

T( cell2mat(v) == 'H', :) = [];

if size(T, 1) > total * 0.05
    out = size(T, 1);
else
    out = 0;
end

end
