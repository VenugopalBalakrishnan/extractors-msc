function [out, imtmp] = glom(imorigin, SVMModelglom1)
nfeature = 3;
sehat = strel('disk', 25);
se = strel('disk', 5);

imtmptmp = imtophat(imorigin(:, :, 2),sehat);
imtmptmp = imadjust(imtmptmp);
imtmp = false(size(imorigin(:,:,1)));
[height, weight] = size(imtmp);

thresh = multithresh(imorigin(height/3:height*2/3, weight/3:weight*2/3, 2),6);

mask2 = im2bw(imtmptmp, min(0.9, double(thresh(6))/255));
mask = im2bw(imorigin(:, :, 2), min(0.9, double(thresh(6))/255));

mask2 = mask2 |mask;


BWbig = bwareaopen(mask2, 5000);
BWbig = bwpropfilt(BWbig, 'MajorAxisLength', [80, 650]);
BWsmall = mask2 & ~BWbig;
BW1 = imclose(BWsmall, se);
BW = BWbig | BW1;

BW = bwareaopen(BW, 200);

BW = bwpropfilt(BW, 'MajorAxisLength', [80, 650]);
BW = bwpropfilt(BW, 'Solidity', [0, 0.6]);
BW = bwpropfilt(BW, 'Eccentricity', [0.2, 1]);

[mask1, n] = bwlabel(BW);

T = zeros(n, 1);
U = zeros(n,5);
s= regionprops(mask1 , 'Solidity', 'Area', 'Centroid',  'Eccentricity', 'MajorAxisLength', 'MinorAxisLength');

U(:, 1:4) = cat(2, cat(1,s.MajorAxisLength), cat(1,s.MinorAxisLength), cat(1,s.Centroid));

boundaries = bwboundaries(mask1);
for submask =1: n   
    
    boundary = boundaries{submask};
    D = pdist2(boundary,boundary);
    T(submask) = max(D(:));
    U(submask, 5) = submask ;
end

v = predict(SVMModelglom1, cat(2, cat(1,s.Solidity), cat(1,s.Eccentricity), T));

U( cell2mat(v) == 'H', :) = [];
U( U(:, 2) < 120 | U(:, 3) <251 | weight - U(:, 3) <251 | U(:, 4) <251 | height - U(:, 4) <251, :) = [];
idx = (cell2mat(v) == 'S'  );
BW3 = ismember(mask1,find(idx));
%%the center of glomeruli
out = zeros(n,2);
k=1;

for subimage = 1:size(U, 1)
    if  nnz(imtmp(int32(U(subimage, 4))-10 : int32(U(subimage, 4))+10, int32(U(subimage, 3))-10 : int32(U(subimage, 3)) +10)) ==0
        
        BW = imcrop(BW3, [int32(U(subimage, 3))-250 int32(U(subimage, 4))-250  500-1 500-1]);
        BW = bwareaopen(BW, 200);
        mask2 = bwlabel(BW);
        s= regionprops(mask2,'MajorAxisLength', 'Centroid', 'Solidity', 'Eccentricity');
        
        BWmax = imcrop(mask1 == U(subimage, 5), [int32(U(subimage, 3))-250 int32(U(subimage, 4))-250  500-1 500-1]);
        
        %  nnz(immultiply(bwconvhull(mask2 == submask), bwconvhull(BWmax))) == 0 && ...
        
        for submask = 1: size(s, 1)
            
            imtmptmp = logical(func_Drawline(zeros(500, 500), 250, 250, int16(s(submask).Centroid(2)), ...
                int16(s(submask).Centroid(1)), 1))& ~ BWmax;
            [~, num] = bwlabel(imtmptmp);
            if (norm([250, 250] - s(submask).Centroid) > max([200, (U(subimage, 1) - U(subimage, 2)/2)]) || ...
                    num > 1)
                BW(mask2 == submask) =0;
            end
            convexhullfinal = bwconvhull(BW);
            
            props= regionprops(convexhullfinal, 'Centroid', 'Area', 'MajorAxisLength','MinorAxisLength' );
            
            
            if  nnz(convexhullfinal) >0 && props.MinorAxisLength/props.MajorAxisLength > 0.45 ...
                    &&  props.Area > 15000  
                out(k, :) = U(subimage, 3:4);
                k = k+1;
                imtmp(int32(U(subimage, 4))-250 : int32(U(subimage, 4))+249, int32(U(subimage, 3))-250 : int32(U(subimage, 3)) +249) = ...
                    imtmp(int32(U(subimage, 4))-250 : int32(U(subimage, 4))+249, int32(U(subimage, 3))-250 : int32(U(subimage, 3)) +249)...
                    | convexhullfinal;
            end
        end
    end
end
out( all(~out,2), : ) = [];

end
