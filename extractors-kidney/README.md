# ncsa.msc.diagnosis


## Docker Build

To build this docker container use

```
docker build -t ncsa.msc.diagnosis .
```

This extractor assumes you will have mounted a completed matlab install at /matlab in the contiainer. You can install matlab on a ubuntu 64 bit machine and copy the resulting /usr/local/MATLAB/&lt;release&gt; folder.

To run the extractor use:

```
docker run --link rabbitmq:rabbitmq --volume ${PWD}/matlab:/matlab ncsa.msc.diagnosis
```

## Reference
Zhao Yan, Edgar F. Black, Luigi Marini, Kenton McHenry, Norma Kenyon, Rachana Patil, Andre Balla Amelia Bartholomew. "Automatic Glomerulus Extraction in Whole Slide Images Towards Computer Aided Diagnosis." eScience (eScience), 2016 IEEE 12th International Conference on. IEEE, 2016.